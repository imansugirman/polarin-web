<?php

function custom_enqueue_styles()
{
    wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css');
    wp_enqueue_style('hover-style', get_stylesheet_directory_uri() . '/css/hover.css');
    wp_enqueue_style('fonts-style', get_stylesheet_directory_uri() . '/css/fonts.css');
    wp_enqueue_style('custom-style', get_stylesheet_directory_uri() . '/css/custom.css');
    // wp_enqueue_style('mobile-style', get_stylesheet_directory_uri() . '/css/mobile.css');
}

add_action('wp_enqueue_scripts', 'custom_enqueue_styles');


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5e2bd0ee18ce3',
	'title' => 'Brand Color',
	'fields' => array(
		array(
			'key' => 'field_5e2bd1efef3ae',
			'label' => 'Color',
			'name' => 'brand_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'pwb-brand',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;