<div class="wrap">
	<?php echo do_shortcode( '[wbcom_admin_setting_header]' ); ?>
	<div class="reign-demos-wrapper">
		<h4 class="wbcom-demo-name"><?php esc_html_e( 'Reign BuddyPress', 'cfup' ); ?></h4>
		<div class="wbcom-demo-content-wrap">
			<div class="wbcom-demo-importer">
				<div class="container">
					<div class="wbcom-image-wrapper">
						<img src="<?php echo CUSTOM_FONT_UPLOADER_PLUGIN_URL . 'admin/wbcom/assets/imgs/reign.jpg'; ?>" alt="Avatar" class="image" style="width:100%">
					</div>
					<div class="wbcom-demo-title">
						<h2><?php esc_html_e( 'REIGN Community', 'cfup' ); ?></h2>
						<ul class="wbcom_theme_features_list">
                            <li><?php esc_html_e( 'Multiple Member Directory Layout', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Multiple Member Header Layout', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Multiple Group Directory Layout', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Multiple Group Header Layout', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'BuddyPress Customization Options', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'BuddyPress Extra Widgets', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Inbuilt Social Profile Module', 'cfup' ); ?></li>
                        </ul>
						<div class="wbcom-middle">
							<a href="https://wbcomdesigns.com/downloads/reign-buddypress-theme/#pricing-table-leranmate" class="wbcom-button wbcom-purchase" target="_blank"><?php esc_html_e( 'Purchase', 'cfup' ); ?></a>
							<a target="_blank" href="http://demos.wbcomdesigns.com/reign-demo1/" class="wbcom-button"><?php esc_html_e( 'Preview', 'cfup' ); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<h4 class="wbcom-demo-name"><?php esc_html_e( 'LearnMate', 'cfup' ); ?></h4>
		<div class="wbcom-demo-content-wrap">
			<div class="wbcom-demo-importer">
				<div class="container">
					<div class="wbcom-image-wrapper">
						<img src="<?php echo CUSTOM_FONT_UPLOADER_PLUGIN_URL . 'admin/wbcom/assets/imgs/learnmate.jpg'; ?>" alt="Avatar" class="image" style="width:100%">
					</div>
					<div class="wbcom-demo-title">
						<h2><?php esc_html_e( 'Learnmate', 'cfup' ); ?></h2>
						<ul class="wbcom_theme_features_list">
                            <li><?php esc_html_e( 'Add Review to your course', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Course Coming Soon', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Grid And List Layout', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Learndash/LifterLMS Extra Widgets', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Unparalleled Course Layout', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Awesome Learndash/LifterLMS Pages', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Distraction Free Reading', 'cfup' ); ?></li>
                        </ul>
						<div class="wbcom-middle">
							<a href="https://wbcomdesigns.com/downloads/learnmate-learndash/#pricing-table-leranmate" class="wbcom-button wbcom-purchase" target="_blank"><?php esc_html_e( 'Purchase', 'cfup' ); ?></a>
							<a target="_blank" href="https://wpbp.in/learndash/" class="wbcom-button" ><?php esc_html_e( 'Preview', 'cfup' ); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<h4 class="wbcom-demo-name"><?php esc_html_e( 'StoreMate', 'cfup' ); ?></h4>
		<div class="wbcom-demo-content-wrap">
			<div class="wbcom-demo-importer">
				<div class="container">
					<div class="wbcom-image-wrapper">
						<img src="<?php echo CUSTOM_FONT_UPLOADER_PLUGIN_URL . 'admin/wbcom/assets/imgs/storemate.jpg'; ?>" alt="Avatar" class="image" style="width:100%">
					</div>
					<div class="wbcom-demo-title">
						<h2><?php esc_html_e( 'ShopMate Dokan', 'cfup' ); ?></h2>
						<ul class="wbcom_theme_features_list">
                            <li><?php esc_html_e( 'Dokan Extra Widgets', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Unique Single Product Page Customization', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Awesome WooCommerce Pages', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Grid And List Layout', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Automated Mega Menu Support', 'cfup' ); ?></li>
                        </ul>
						<div class="wbcom-middle">
							<a target="_blank" href="https://wbcomdesigns.com/downloads/storemate-dokan/#pricing-table-leranmate" class="wbcom-button wbcom-purchase"><?php esc_html_e( 'Purchase', 'cfup' ); ?></a>
							<a target="_blank" href="https://demos.wbcomdesigns.in/reign-dokan/" class="wbcom-button"><?php esc_html_e( 'Preview', 'cfup' ); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<h4 class="wbcom-demo-name"><?php esc_html_e( 'Jobmate', 'cfup' ); ?></h4>
		<div class="wbcom-demo-content-wrap">
			<div class="wbcom-demo-importer">
				<div class="container">
					<div class="wbcom-image-wrapper">
						<img src="<?php echo CUSTOM_FONT_UPLOADER_PLUGIN_URL . 'admin/wbcom/assets/imgs/jobmate.jpg'; ?>" alt="Avatar" class="image" style="width:100%">
					</div>
					<div class="wbcom-demo-title">
						<h2><?php esc_html_e( 'Jobmate Theme', 'cfup' ); ?></h2>
						<ul class="wbcom_theme_features_list">
                            <li><?php esc_html_e( 'Simple live searching and filtering', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Unique Job listing Page', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Awesome Resume list Page', 'cfup' ); ?></li>
                            <li><?php esc_html_e( 'Outstanding all job maanger pagesLayout', 'cfup' ); ?></li>
                        </ul>
						<div class="wbcom-middle">
							<a target="_blank" href="https://wbcomdesigns.com/downloads/jobmate-theme/" class="wbcom-button wbcom-purchase"><?php esc_html_e( 'Purchase', 'cfup' ); ?></a>
							<a target="_blank" href="http://demos.wbcomdesigns.in/jobmate-theme/" class="wbcom-button"><?php esc_html_e( 'Preview', 'cfup' ); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>